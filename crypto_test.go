// Copyright 2016 Jean-Nicolas Moal. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package gcw

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"testing"
)

var wrongKey = []byte("wrongKey")
var goodKey []byte

func init() {
	//16 bits key
	goodKey = make([]byte, 16)
	_, err := rand.Read(goodKey)
	if err != nil {
		panic(err)
	}
}

func TestEncryptDecrypt(t *testing.T) {
	var err error
	data := []byte("Data to encrypt")
	var encrypted bytes.Buffer
	c, err := NewCrypto(goodKey)
	if err != nil {
		t.Error(err)
	}
	c.Encrypt(bytes.NewBuffer(data), &encrypted)

	var result bytes.Buffer
	err = c.Decrypt(&encrypted, &result)
	if err != nil {
		t.Error(err)
	}

	if bytes.Compare(data, result.Bytes()) != 0 {
		t.Error(fmt.Errorf("encrypt/decrypt failed : expecting [%s] got [%s]", data, result.Bytes()))
	}
}
