// Copyright 2016 Jean-Nicolas Moal. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package gcw is a convenient package for encryption.
package gcw

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"io"
	"io/ioutil"
)

//Crypto allows data encryption
type Crypto struct {
	key   []byte       // The key used for encryption
	block cipher.Block // The cipher block used for encryption
}

// NewCrypto creates a new crypto
func NewCrypto(key []byte) (c *Crypto, err error) {
	c = new(Crypto)
	c.key = key
	c.block, err = aes.NewCipher(key)
	if err != nil {
		c = nil
	}

	return
}

// Encrypt encrypts the content of r into w
func (c *Crypto) Encrypt(r io.Reader, w io.Writer) (err error) {
	var cipherText []byte
	var data []byte
	data, err = ioutil.ReadAll(r)
	if err != nil {
		return
	}
	cipherText = make([]byte, aes.BlockSize+len(string(data)))

	// iv (initialization vector) is used along the key to make sure
	// that the same text is not encrypted with the same value.
	iv := cipherText[:aes.BlockSize]
	// rand.Reader is a cryptographically secure pseudorandom number generator. see golang doc.
	_, err = io.ReadFull(rand.Reader, iv)
	if err != nil {
		return
	}

	cfb := cipher.NewCFBEncrypter(c.block, iv)
	cfb.XORKeyStream(cipherText[aes.BlockSize:], data)

	enc64 := base64.StdEncoding.EncodeToString(cipherText)
	_, err = w.Write([]byte(enc64))

	return
}

// Decrypt decrypts content from r and writes it into w.
func (c *Crypto) Decrypt(r io.Reader, w io.Writer) (err error) {
	var block cipher.Block
	var data []byte
	var rawData []byte
	var plainText []byte

	rawData, err = ioutil.ReadAll(r)
	if err != nil {
		return
	}

	data, err = base64.StdEncoding.DecodeString(string(rawData))

	block, err = aes.NewCipher(c.key)
	if err != nil {
		return
	}

	iv := data[:aes.BlockSize]
	data = data[aes.BlockSize:]

	cfb := cipher.NewCFBDecrypter(block, iv)
	plainText = make([]byte, len(data))
	cfb.XORKeyStream(plainText, data)

	_, err = w.Write(plainText)
	return
}
