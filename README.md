# GCW (Go Crypto Wrapper)

This package aims to ease the use of the golang crypto library package. For now, only AES encryption is available through this package.

[![build status](https://gitlab.com/jn-moal/gcw/badges/master/build.svg)](https://gitlab.com/jn-moal/gcw/commits/master)
[![coverage report](https://gitlab.com/jn-moal/gcw/badges/master/coverage.svg)](https://gitlab.com/jn-moal/gcw/commits/master)

## Licensing
The source files are distributed under the BSD-style license available in the license file.

## Quickstart
Simply go get this package (go get gitlab.com/jn-moal/gcw).

## Documentation:

* [Documentation on godoc.org](https://godoc.org/gitlab.com/jn-moal/gcw)

## Contributions
Feel free to create an issue if you have a question or found a bug.
