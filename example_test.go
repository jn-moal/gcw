// Copyright 2016 Jean-Nicolas Moal. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package gcw_test

import (
	"bytes"
	"crypto/rand"
	"fmt"

	"gitlab.com/jn-moal/gcw"
)

func Example() {
	data := []byte("Data to encrypt")
	var encrypted bytes.Buffer
	goodKey := make([]byte, 16)

	// Here we are using a random key of 16 bytes.
	// The key can be either 16, 24 or 32 bytes,
	// see golang aes documentation.
	cryptoKey := make([]byte, 16)
	_, err := rand.Read(goodKey)
	if err != nil {
		fmt.Println("Could not create the key")
	}

	c, err := gcw.NewCrypto(cryptoKey)
	if err != nil {
		fmt.Println("Could not instanciate the crypto")
	}

	// The API is based on the io.Writer and io.Reader,
	// this way it's easier to encrypte/decrypt from/to file.
	err = c.Encrypt(bytes.NewBuffer(data), &encrypted)
	if err != nil {
		// Encryption has failed
	}
}
